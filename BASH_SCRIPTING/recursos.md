### Recursos per a bash scripting

* [Llicències de programari lliure](https://speakerdeck.com/cesarvaliente/libre-open-source-software)
* [Curs de shell scripting](https://ia601407.us.archive.org/30/items/cursshellscript-catala/cursshellscript-catala.pdf)
* [Learn the linux command line](http://linuxcommand.org/index.php)
* [Recomanacions molt interessants](http://wiki.bash-hackers.org/scripting/debuggingtips)
* [Col·lecció d'scripts de nivell avançat](https://github.com/brandonprry/wicked_cool_shell_scripts_2e)
* A **T**he **L**inux **D**ocumentation **P**roject (**tldp**.org) hi ha molta documentació interessant:
  + [Bash Guide for Beginners](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/)
  + [BASH Programming - Introduction HOW-TO](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html)
  + [Advanced Bash-Scripting Guide](http://tldp.org/LDP/abs/html/)

