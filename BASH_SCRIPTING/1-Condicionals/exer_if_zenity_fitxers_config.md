## Mini exercici estructura condicional.
       
*Es necessita haver vist el condicional, potser és un bon exemple per
utilitzar-lo just quan es comença amb els condicionals*
       
##### 1. Instal·lació

Instal·leu `fortune`, un programa que mostra *frases cèlebres* (feu abans `apt-cache search fortune`)

##### 2. Test

Proveu-lo.
       
```
fortune
Calling you stupid is an insult to stupid people! 
	-- Wanda, "A Fish Called Wanda"
```

##### 3. Test gràfic

Mostreu-lo ara gràficament. Volem que es mostri una finestra com aquesta:

![frase de fortune](fortune.png)

##### 4. Fitxer d'inicialització

A quin fitxer hauria de posar l'ordre que executa fortune perquè cada vegada que entressiu a la vostra sessió gràfica es mostrés un missatge de `fortune`?

##### 5. Joc de proves

Que passa si entro a una consola de text (Alt + F2)?

Intenta mostrar-ho però com que no hi ha sessió gràfica no es pot llençar `zenity`.
 
Demaneu pista al profe si ho necessiteu.

##### 6. Millores

+ L'ordre `zenity` espera interacció per part de l'usuari, per exemple que
premi el botó d'OK abans de continuar amb el següent procés. Com aconseguim que
el sistema continui carregant els diferents processos mentre es mostra la
finestra de zenity?

+ Inicialment la finestra de zenity no té vores ni altres elements estètics.
Perquè creus que passa i com es pot solucionar això?

##### Nota

Si és necessari canvieu a l'entrada de Gnome (gdm) el servidor gràfic: de
*Wayland*  (el que hi ha per defecte a *gnome*) a *Xorg*.

---

Tags: *zenity, .bashrc, .profile, if, DISPLAY, fortune*
