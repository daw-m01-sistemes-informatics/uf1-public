# Arguments rebuts per un script

Suposem que el nostre script ja té els permisos d'execució.
Podem fer que el nostre script utilitzi diferents arguments
cada cop que s'executi passant-li aquests arguments per la línia d'ordres.

1. Començarem amb un script molt senzill que mostri els 3 arguments que li passo per la línia d'ordres.
És a dir, que el resultat de l'execució sigui semblant a:
	```
	[user@localhost ~]$ ./mostra_arguments.sh hola que tal
	el valor de l'argument 1 és hola
	el valor de l'argument 2 és que
	el valor de l'argument 3 és tal
	```
	_hint: echo, $1 ..._
2. L'script mostrarà ara també el nom de l'script i el total d'arguments que li hem passat.
El resultat de l'execució ara serà semblant a:
	```
	[user@localhost ~]$ ./mostra_arguments.sh hola que tal noi
	el nom del programa és ./mostra_arguments.sh
	el valor de l'argument 1 és hola
	el valor de l'argument 2 és que
	el valor de l'argument 3 és tal
	el valor de l'argument 4 és noi
	el número d'arguments és 4
	```
3. Ara ens interessa saber com afecta als arguments l'execució de l'ordre shift.
Per a això, en aquest script torna a escriure les ordres de l'exercici 2, després executa shift
i a continuació torna a repetir les ordres de l'exercici 2 un altre cop.
Observant la sortida de l'script quines conclusions pots treure?

tags: $0, $1, $2, $3, $4 ..., $#, $\*, $@, shift
