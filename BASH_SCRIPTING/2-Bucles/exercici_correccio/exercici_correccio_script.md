##### Enunciat

Un estudiant de *DAW* no ha passat una prova a una entrevista de treball. La
veritat és que no tenim ni idea del que li demanaven però entenem que no
l'hagin acceptat: l'script no funciona, no segueix les recomanacions de
llegibilitat, no hi ha comentaris, identacions horribles ... la seva lectura
pot provocar marejos i per tant tota prevenció és poca. Molta cura sisplau.


Heu de respondre a les seguents qüestions:

- Que intenta fer l'[script](trtrtr.sh)

- Corregir-lo perquè funcioni:

	* afegint una bona capçalera i **descripció**
	* afegint **comentaris**
	* canviant el nombre de les variables (que siguin **descriptives**)
	* corregint **identacions**
	* corregir instruccions perquè finalment funcioni

##### Observacions

Recordeu que per depurar errors podeu fer:

```
sh -x nomScript.sh
```

Una altra manera de depurar és comentar línies. Si voleu comentar una línia,
recordeu que es fa amb:

```
# línia comentada
```

Si volem comentar un bloc sencer, penseu que la forma *No estàndard* de
comentar un bloc de línies és utilitzar el here-document.

Afortunadament podem comentar un bloc sencer de línies amb una expressió
regular que afegeixi principi de línia un *#* des de la línia *m* a la línia
*n*. Dintre del vim és relativament fàcil fer això.


