

# Tasques a realitzar per a evitar problemes amb els fitxers de diferents sistemes.

Quan transferim dades de l'escola a casa (i viceversa) estem copiant, retallant i movent fitxers entre diferents sistemes de fitxers, amb els problemes que això pot comportar (de permisos, problema del *case sensitive* amb els noms dels fitxers,...)

Pensem que aquestes transferències de dades es donen entre 3 sistemes de fitxers diferents:

* el sistema de fitxers de la partició del disc dur del pc de l'Escola,
* el sistema de fitxers de la partició del disc dur del pc de casa (o portàtil)
* el sistema de fitxers de la partició del dispositiu per emmagatzemar i transportar aquesta informació 
(pensarem que usualment es tracta d'una memòria flash pen USB).


## Mateix uid a casa que a l'escola.



Primer de tot mirem quin és el nostre numero identificador a l'escola com a usuari ordinari iawxxxxxxx, amb la següent ordre:

```
[iawxxxxxx@hxx ~]$ id
uid=(200345)iawxxxxxx gid=...
```

Aquesta ordre ens proporciona diferents identificadors. El que ens interessa a nosaltres ara és el primer valor: el uid. Aquest valor numèric és el que haurà de coincidir amb el del nostre sistema de casa. Adonem-nos que no cal que el login de casa sigui el mateix que el de l'escola. En efecte, el login de casa pot ser *pep* en comptes del login *iawxxxxx* que tenim a l'escola, l'important és el *uid*.

Entrem **al Fedora de casa** però com a usuari *root*. Ens assegurem que no està oberta la sessió del nostre usuari ordinari de casa (el *pep* que dèiem abans.) Si el sistema insisteix que el nostre usuari ordinari està dintre del sistema sempre podem reiniciar la màquina per assegurar-nos.

Finalment com a usuari root executem la següent ordre:
```
usermod -u 123456 pep
```
(sempre i quan *123456* sigui el vostre numero de uid que teniu a l'escola i *pep* l'usuari que teniu creat a casa)




## Particionament memòria flash pen USB

Tot i que per viure bé no cal tenir particions vfat pels sistemes de Microsoft, crearem una, no sigui que ens trobem a un cibercafè de Bora Bora i l'únic ordinador disponible tingui aquest sistema.

Hi ha un petit problema amb certes versions del Windows de Microsoft (hi ha gent que considera que aquest sistema operatiu té molts més problemes, però aquesta discussió no ve al cas). El problema consisteix en la impossibilitat de crear particions a un pen USB, i com a conseqüència aquests sistemes només reconeixen una partició a un pen USB, que ha de ser la PRIMERA.

Per tant, si volem crear més d'una partició a un memòria flash USB, la PRIMERA partició ha de ser la que volem que sigui visible pels sistemes windows. En funció de la mida del nostre pen USB, farem de més o de menys.

Una opció molt completa, si hi ha suficient espai, és tenir 3 particions:

- Una primera partició VFAT (que contindrà dades) per si ens trobem en la necessitat d'utilitzar un sistema windows.

- Una segona partició VFAT amb un Fedora Live per reinstal·lar, recuperar o utilitzar en altres ordinadors un GNU/Linux d'una manera segura.

- Una 3a partició EXT4 (que contindrà dades). La partició habitual on desarem els nostres fitxers de l'Escola.

La recomanació seria:

- 1100 MB per la partició on hi haurà el Fedora Live 20

- Un mínim de 400MB per la VFAT de dades.

- Un mínim de 400MB per la ext4 de dades.

- Es bona idea, també posar una *LABEL* informativa a cadascuna de les particions: per exemple DADES\_FAT a la 1a i DADES\_EXT4 per a la 3a.

Si el pen és més petit de 2 GB, no fer el Fedora Live: només 2 particions amb dades ext4 i dades FAT32.

Farem les particions amb *gparted*, una eina gràfica de particionament que també permet redimensionar particions ja existents. Per defecte no s'instal·la a Fedora i haurem d'instal·lar-la nosaltres.

Com a root fem:
```
yum install gparted -y
```
Un cop instal·lat ja podem executar-la com a root fent:
```
gparted
```

Hem de tenir en compte que aquesta eina és molt poderosa i per tant si no la fem servir adequadament podem perdre moltes dades importants. Recordem fer-ho com a root i sobretot fixem-nos molt i molt bé en el dispositiu que estem escollint (no sigui que intentem particionar el disc dur en comptes de la memòria flash !!!)

Podem utilitzar *gparted* d'una maner més segura, dient-li que s'activi només per al nostre dispositiu pen USB. Per fer això és necessari saber quin és el nom del nostre dispositiu pen USB, si sabem del cert que només hi ha un disc dur i el nostre pen USB, aquest serà /dev/sdb però per estar segurs és millor utilitzar l'ordre mount per veure com a muntat el sistema la nostra memòria USB.

Suposant que el nostre dispositiu estigués detectat com a /dev/sdb llavors l'ordre a emprar seria:
```
gparted /dev/sdb
```
(Si no ens permet jugar amb segons quines particions: eliminar totes les particions del pen usb i crear una taula de particions nova)



## Creació d'una partició Fedora Live al pen USB

Necessitem, tal com s'explica abans, tenir al menys una partició (amb un sistema de fitxers ext4 o FAT32) de 1GB (de fet per Fedora 20 una mica més: 1100 MB ).

També és necessari el programa liveusb-creator, l'ordre d'instal·lació com a root és:
```
yum install liveusb-creator -y
```

L'últim element necessari és una imatge ISO del live que volem construir. Per exemple si volguéssim un Fedora 20 Live Desktop 64 bits ens hauríem de baixar la imatge d'aquest [enllaç](https://torrent.fedoraproject.org/torrents/Fedora-Live-Desktop-x86_64-20.torrent)

Si teniu algun problema amb l'anterior enllaç o voleu alguna altra distribució aneu a [la llista de tots els torrents](https://torrents.fedoraproject.org/)

Nosaltres a classe podrem agafar la iso de gandhi mitjançant el protocol ftp amb la següent ordre:
```
wget ftp://gandhi:/pub/iso/Fedora-Live-Desktop-x86_64-20-1.iso
```
Un cop teniu aquests elements **com a root** executarem:
```
liveusb-creator   --reset-mbr
```
indicarem on es troba la imatge del Fedora i, ULL VIU,també hem de dir a quina partició voleu instal·lar-la.

## Canvi del propietari de la partició ext4 del pen USB

Com a root heu de fer que el propietari del pen sigui l'usuari  iawxxxxx

Si per exemple l'etiqueta del pen es DADES\_EXT4

l'ordre, seria:
```
chown    -R     iawxxxxx      /run/media/iawxxxxx/DADES_EXT4
```
(change owner: canvi de propietari   -Recursivament iawxxxx serà el nou propietari del directori /run/media/iawxxxxx/DADES_EXT4)


Alehop !



Es pot treballar amb altres eines gràfiques com Unetbootin o d'altres: més info [aquí](https://fedoraproject.org/wiki/How_to_create_and_use_Live_USB).

vfat = notació linuxera dels sistemes de fitxers FAT16, FAT32


