## L'índex (staging area)


Anem a afegir els fitxers que volem que se'ls faci un seguiment (tracking)   
Provem amb un:

```
[pingui@localhost intro-git]$ git add 01-primers_passos.md
```

Si executem:

```
[pingui@localhost intro-git]$ git status
```
Es mostrarà alguna cosa semblant a:

```
 On branch master

    Initial commit

    Changes to be committed:
    (use "git rm --cached <file>..." to unstage)

	   new file:   01-primers_passos.md

     Untracked files:
    (use "git add <file>..." to include in what will be committed)

  	esquema_projecte_git.png
  	esquema_projecte_git.md
```
[Canvis a l'índex](03-primers_passos.md)

