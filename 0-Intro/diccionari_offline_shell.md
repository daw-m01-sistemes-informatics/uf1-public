Instal·lem el corrector aspell i els diccionaris de català castellà i anglès:

```
apt install aspell aspell-ca aspell-es aspell-en
```

Si no tenim un fitxer per corregir, creem un:

```
echo "Setse jutges menjen fetje d'un penjat. Si el jutjat es despengés, es menjaria els setze fetges dels setze jutges que l'han jutjat." > embarbussament.txt
```

Passem el corrector de l'idioma del fitxer:
```
aspell --lang=ca check embarbussament.txt
```
