## Utilització de la càmera del mòbil com a webcam

Si no disposem d'una webcam, però tenim càmera del mòbil, podem fer-la servir
amb una aplicació com _droidcam_

#### Instal·lació de l'app al mòbil

Si tens un android o un iphone no té gaire secret:

[link de droidcam](https://www.dev47apps.com/)

#### Instal·lació de l'aplicació al teu escriptori

Ho expliquem per a Fedora:

##### Comprovació/Verificació d'eines necessàries


És recomanable fer un `dnf update` i en el cas de que uns dels paquets que
s'actualitzi sigui el kernel haurem de reiniciar el sistema.

Comprovem que tenim `gcc`, `make`, i els `devel` del vostre `kernel`. Us haurien de
sortir els 3 elements amb aquesta ordre:

```
rpm -qa | grep "^gcc\|^make\|kernel-devel-`uname -r`"

```

Si falta algun, l'instal·leu amb l'ordre habitual dnf

```
dnf install gcc  make  kernel-devel-`uname -r`
```

Assegurem-nos que si ja tenim droidcam, estigui apagat.


##### Descàrrega, compilació i instal·lació de la darrera versió del programa

Finalment executem les següents instruccions una a una assegurant-nos que
s'executen bé:

```
cd /tmp/
wget https://files.dev47apps.net/linux/droidcam_latest.zip
unzip droidcam_latest.zip -d droidcam
cd droidcam && sudo ./install-client
```

#### Ús de l'aplicació

Fem servir el mode en que el portàtil i el mòbil estan connectats a una mateixa
xarxa d'àrea local LAN (wifi o no). [Existeixen altres
modes](https://www.dev47apps.com/droidcam/connect/)


Executeu l'app del mòbil i quan us digui quina és la IP del mòbil la guardeu. 

Ara executeu des d'una terminal de l'ordinador:

```
droidcam
```

Es mostrarà una finestra amb un camp _Phone IP_ a on posarem la IP que teníem
guardada d'abans.

![finestra de configuració de droidcam](Droidcam.png)


Ja ens hauríem de veure.



#### Troubleshouting

- No tinc `sudo`!

	Cap problema. Com a root, afegeix el teu usuari al grup `wheel` i
	després surt de la sessió perquè el sistema llegeixi el teu nou grup:

	```
	gpasswd -a el_teu_usuari  wheel
	```

	Recorda sortir de la sessió per poder fer servir `sudo`

- Tinc la càmera trasera i vull la frontal (o a l'inrevés).
	
	Tanca la sessió, obre les preferències (settings) de l'aplicació
	droidcam al mòbil i canvia per l'opció adient.


- La instal·lació ha canviat de manera que podria ser necessari instal·lar `sudo ./install-video` i `sudo ./install-sound.`
	
	Comprovar-ho a [la web d'instal·lació de droidcam](https://www.dev47apps.com/droidcam/linux/)



