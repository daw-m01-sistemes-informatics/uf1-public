### Exercicis fets amb Debian 11 (novembre 2021)


##### Exercici 1.
Responeu a la pregunta sense provar-ho a la terminal: què es mostrarà per pantalla si executo:

```
[arale@vila_del_pingui ~]$ echo -n "modul 01" || echo "UF1" && echo -e "\nSistemes Informàtics"
```

##### Exercici 2.
Executa només la primera ordre i endevina que mostrarà la segona:
```
echo $BASH_SUBSHELL;
(sleep 1; echo $BASH_SUBSHELL; sleep 1)
```

##### Exercici 3.
Feu el listing 9 afegint l'ordre `sleep 10` al final, de manera que quedi com:

```
[arale@vila_del_pingui ~]$ bash -c "echo Expand in parent $$ $PPID; sleep 10"
```

Abans de que no s'esgotin els 10 segons executeu en una altra terminal la següent comanda:

```
pstree -pha
```

Mireu de trobar el procés *sleep*


##### Exercici 4.
Estic al bash i he executat:

```
[arale@vila_del_pingui ~]$ animal="cat"
```
o potser
```
[arale@vila_del_pingui ~]$ animal="dog"
```
de fet no ho recordo bé, però vull que em mostri per pantalla el plural en anglès de l'animal que hi hagi a la variable.
Quina ordre executaries?

##### Exercici 5.
Si obro una terminal i escric:
```
[arale@vila_del_pingui ~]$ exec sleep 10
```
que succeeix? ho podries explicar?

##### Exercici 6.

Dona una ordre amb la qual pugui veure la meva arquitectura (bé, de fet la del
meu pc). I la del nucli? i si vull una info completa del sistema (un breu
resum)

---
**Històric**

##### Exercici 7.

Quantes línies es desen a l'històric de comandes a memòria ? Troba la variable
d'entorn que conté aquesta informació, o sigui el número de línies de
l'històric a memòria. (hint: history, help ...)

##### Exercici 8. 

I al fitxer històric de comandes ? Troba la variable d'entorn que conté aquesta
informació (el número de línies del fitxer històric).

##### Exercici 9. 

Amb quina combinació de tecles faig una cerca inversa i puc trobar la darrera
vegada que he utilitzat una ordre que conté un cert patró ? Com trobo
l'anterior ocurrència ?

##### Exercici 10. 

En aquest exercici no heu de resoldre res, només executar les ordres que
s’enumeren, però si no feu el següent exercici que resol el que heu fet en
aquest, no podreu accedir al programa java de veritat i per tant el DrJava que
utilitza aquesta ordre java no funcionarà.

Escriviu a la terminal:

```
java
```

veureu que 

```
echo $PATH
```

Se us mostren una sèrie de directoris separats pel caràcter ":"

Per exemple una sortida podria ser:

```
/home/pingui/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
```

*ALERTA: Aquest exercici només funciona si al 1er directori del vostre PATH no
es troba un executable de nom java.*

Com a root fem el següent:
```
[root@pc666 ~]# vim /usr/local/bin/java
```
i dintre escrivim les següents línies:
```
#!/bin/bash
# Script que mostra la data
date
```
Tanquem i desem el fitxer. Com a root donem permís d'execució:
```
[arale@vila_del_pingui ~]$ chmod +x /usr/local/bin/java
```
I ara tornem a l'usuari ordinari iawxxxxxx i executeu:
```
[iawxxxxxx@pc666 ~]$ java
```
##### Exercici 11.

Arreglem el problema creat abans.

##### Exercici 12.

Ara treballaré amb un àlies (alerta amb les cometes _no escapades_):

```
[arale@vila_del_pingui ~]$ alias java="echo Java és una illa d\'Indonèsia"
```

executeu ara:

```
[iawxxxxxx@pc666 ~]$ java
```

És necessari eliminar aquest alies ? Tant si ho és com si no, digues com s'elimina un alies:

##### Exercici 13.

Suposem que volem instal·lar-nos *Live Debian 11 Gnome* de 64 bits des [d'aquesta
pàgina](http://ftp.caliu.cat/debian-cd/11.1.0-live/amd64/iso-hybrid/debian-live-11.1.0-amd64-gnome.iso)
i volem assegurar-nos de que la descàrrega no hagi tingut cap error. Com ho
faríem?

El nostre sistema tindra alguna ordre relacionada amb aquest SHA256 per
calcular el checksum?  Investigueu amb quina ordre podem calcular aquesta
cadena per veure si ens hem baixat bé la iso. (hint: apropos)

