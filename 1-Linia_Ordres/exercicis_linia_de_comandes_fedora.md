### Exercicis fets amb Fedora 32 (novembre 2020)


##### Exercici 1.
Responeu a la pregunta sense provar-ho a la terminal: què es mostrarà per pantalla si executo:

```
[arale@vila_del_pingui ~]$ echo -n "modul 01" || echo "UF1" && echo -e "\nSistemes Informatics"
```

##### Exercici 2.
Executa només la primera ordre i endevina que mostrarà la segona:
```
echo $BASH_SUBSHELL;
(sleep 1; echo $BASH_SUBSHELL; sleep 1)
```

##### Exercici 3.
Feu el listing 9 afegint una ordre "sleep 10" al final, de manera que es llegeixi:

```
[arale@vila_del_pingui ~]$ bash -c "echo Expand in parent $$ $PPID; sleep 10"
```

Abans de que no s'esgotin els 10 segons executeu en una altra terminal la següent comanda:

```
pstree -pha
```
Mireu de trobar el procés *sleep*

##### Exercici 4.
Estic al bash i he executat:

```
[arale@vila_del_pingui ~]$ animal="cat"
```
o potser
```
[arale@vila_del_pingui ~]$ animal="dog"
```
de fet no ho recordo bé, però vull que em mostri per pantalla el plural en anglès de l'animal que hi hagi a la variable.
Quina ordre executaries?

##### Exercici 5.
Si obro una terminal i escric:
```
[arale@vila_del_pingui ~]$ exec sleep 10
```
que succeeix? ho podries explicar?

---
**Històric**

##### Exercici 6.
Dona una ordre amb la qual pugui veure la meva arquitectura (bé, de fet la del meu pc). I la del nucli? i si vull una info completa del sistema (un breu resum)

##### Exercici 7.
Quantes línies es desen a l'històric de comandes a memòria ? Troba la variable d'entorn que conté aquesta informació, o sigui el número de línies de l'històric a memòria. (hint: history, apropos, help ...)

##### Exercici 8. 
I al fitxer històric de comandes ? Troba la variable d'entorn que conté aquesta informació (el número de línies del fitxer històric).

##### Exercici 9. 
Es desen les ordres consecutives duplicades a l'històric de comandes? Quina variable conté aquesta informació? Doneu una solució fàcil perquè sí que emmagatzemi les ordres consecutives repetides.
Intenteu esbrinar a quin fitxer es troba aquesta variable.  

##### Exercici 10. 
Amb quina combinació de tecles faig una cerca inversa i puc trobar la darrera vegada que he utilitzat una ordre que conté un cert patró ? Com trobo l'anterior ocurrència ?

##### Exercici 11. 
En aquest exercici no heu de resoldre res, només executar les ordres que s’enumeren, però si no feu el següent exercici que resol el que heu fet en aquest, no podreu accedir al programa java de veritat i per tant el DrJava que utilitza aquesta ordre java no funcionarà.

Feu:
```
echo $PATH
```
Se us mostren una sèrie de directoris separats pel caràcter ":"

Per exemple una sortida podria ser:
```
/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/home/leviatan/.local/bin:/home/leviatan/bin
```
*ALERTA: Aquest exercici només funciona si al 1er directori del vostre PATH no es troba un executable de nom java.*

Com a root fem el següent:
```
[root@pc666 ~]# vim /usr/local/bin/java
```
i dintre escrivim les següents línies:
```
#!/bin/bash
# Script que mostra la data
date
```
Tanquem i desem el fitxer. Com a root donem permís d'execució:
```
[arale@vila_del_pingui ~]$ chmod +x /usr/local/bin/java
```
I ara tornem a l'usuari ordinari iawxxxxxx i executeu:
```
[iawxxxxxx@pc666 ~]$ java
```
##### Exercici 12.
Arreglem el problema creat abans.

##### Exercici 13.
Ara treballaré amb un àlies (alerta amb les cometes _no escapades_):
```
[arale@vila_del_pingui ~]$ alias java="echo Java és una illa d\'Indonèsia"
```
executeu ara:
```
[iawxxxxxx@pc666 ~]$ java
```
És necessari eliminar aquest alies ? Tant si ho és com si no, digues com s'elimina un alies:

##### Exercici 14.

Suposem que volem instal·lar-nos *Live Fedora32* de 64 bits des [d'aquesta pàgina](http://mirror.uv.es/mirror/fedora/linux/releases/32/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-32-1.6.iso) i després volem assegurar-nos de que la baixada no ha tingut errors. Com ho faríem?

Al mateix directori que conté la iso trobem el fitxer:

Fedora-Workstation-32-1.6-x86_64-CHECKSUM

i dintre la línia:

```
SHA256 (Fedora-Workstation-Live-x86_64-32-1.6.iso) = 4d0f6653e2e0860c99ffe0ef274a46d875fb85bd2a40cb896dce1ed013566924
```

> Una función hash es un algoritmo que transforma ("digiere") un conjunto arbitrario de elementos de datos, como puede ser un fichero de texto, en un único valor de longitud fija (el "hash").

Extret d'[aquest enllaç de l wikipedia](https://es.wikipedia.org/wiki/SHA-2)

El nostre sistema tindra alguna ordre relacionada amb aquest SHA256 per calcular el checksum?
Investigueu amb quina ordre podem calcular aquesta cadena per veure si ens hem baixat be la iso. (hint: apropos)

#### Extra

+ [Shell variables & Environment variables](https://web.archive.org/web/20170713001430/http://sc.tamu.edu/help/general/unix/vars.html)

+ [Subshell vs Subprocess](https://jitpaul.blog/2018/09/16/shell-scripting-sub-shell-vs-sub-process/)
