### BACKUP VIM

Editem a una consola de text un fitxer qualsevol, per exemple:

```
vim fitxer.txt
```

Escrivim qualsevol cosa, per exemple:
```
Aquest és un fitxer de prova
```
però el més important: **no deseu el fitxer!!!**

Si us equivoqueu, desant, elimineu el fitxer i torneu a repetir les instruccions anteriors.

Anem a esbrinar si s'ha creat un backup (còpia de seguretat) del nostre fitxer.
Farem un cop d'ull al directori on es troba el fitxer a veure si trobem algun altre fitxer
que ens pugui semblar un backup del nostre fitxer "fitxer.txt".

Busquem-lo a una altra consola. Recordem que podem llistar els fitxers que tenim amb l'ordre ```ls```.

Potser el fitxer és un fitxer ocult, recordeu d'una banda com comencen els fitxers ocults
i d'altra banda penseu amb quina ordre puc consultar l'ajuda.
```
ls -la  # se suposa que em trobo al directori on estic creant el fitxer amb vim
```

*Heu trobat el fitxer backup?*
Sí:
```
.fitxer.txt.swp
```

Feu un cop d'ull al seu contingut amb l'ordre cat.


Ara reiniciarem la màquina. Abans però, si teniu algun document important sense desar, deseu-lo,
però el fitxer *fitxer.txt* **no l'heu de desar**
Si us equivoqueu, desant el fitxer *fitxer.txt* , elimineu el fitxer i torneu a repetir aquesta part.

Un cop hem reiniciat, intentem obrir el mateix fitxer *fitxer.txt* i **llegim** que diu l'ajuda.


* Quina creeus que sera l'opció adient si volem desar les modificacions?
* Si ara sí que deso i torno a obrir, que passa?
* Com ho puc solucionar?


Per cert, si faig el mateix que abans amb un nou arxiu *fitxer2.txt*
i edito escrivint qualsevol cosa, per exemple ```Aquest també és un fitxer de prova```
i **no deso**. Intenteu ara obrir el mateix fitxer però a una altra consola. 

* Que succeeix?
* I si modifico el fitxer en la 2a consola i no deso els canvis, 
però després intento obrir una 3a consola que passarà?
* Mireu que passa amb el fitxer temporal?
